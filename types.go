package arlog

type LogEntry struct {
	DateTime  []byte;
	File      []byte;
	Namespace []byte;
	Level     Level;
	Message   []byte;
}

type Level int32;

type Levels map[string]Level;

type LevelNames map[Level]string;

type Parser interface {
	Parse([]byte) *LogEntry
}
type Formatter interface {
	Format(*LogEntry) string
}
type Handler interface {
	Handle(*LogEntry) error
}
type Filter interface {
	Filter(*LogEntry) *LogEntry
}
