package arlog

import (
	"testing"
	"log"
	"bytes"
)

func Test_dateLength(t *testing.T) {
	if l := dateLength(log.Ldate | log.Ltime); (l != len("2009/01/23 01:23:23")) {
		t.Errorf("Expected %d, got %d", len("2009/01/23 01:23:23"), l)
	}
	if l := dateLength(log.Ldate | log.Ltime | log.Lmicroseconds); (l != len("2009/01/23 01:23:23.123123")) {
		t.Errorf("Expected %d, got %d", len("2009/01/23 01:23:23.123123"), l)
	}
}

func Test_ParseDate(t *testing.T) {
	s := "2009/01/23 01:23:23.123123 /a/b/c/d.go:23: message"
	if a := parseDate([]byte(s), log.Ldate | log.Ltime); !bytes.Equal([]byte(a), []byte("2009/01/23 01:23:23")) {
		t.Errorf("Expected %s, got %s", "2009/01/23 01:23:23", a)
	}
	if a := parseDate([]byte(s), log.Ldate | log.Ltime | log.Lmicroseconds); !bytes.Equal([]byte(a), []byte("2009/01/23 01:23:23.123123")) {
		t.Errorf("Expected %s, got %s", "2009/01/23 01:23:23.123123", a)
	}
}

func Test_parseEntry(t *testing.T) {
	l := "2009/01/23 01:23:23.123123 /a/b/c/d.go:23: [namespace] info: message"
	expected := LogEntry{
		DateTime: []byte("2009/01/23 01:23:23.123123"),
		File: []byte("/a/b/c/d.go:23"),
		Namespace: []byte("namespace"),
		Level: 3,
		Message: []byte("message"),
	}
	actual := parseEntry([]byte(l), log.Ldate | log.Lmicroseconds | log.Llongfile, levels, 0)
	if !cmpEntry(actual, &expected) {
		t.Errorf("Expected %s, got %s", expected, actual)
	}
}

func cmpEntry(a, b *LogEntry) bool {
	return bytes.Equal(a.DateTime, b.DateTime) &&
		bytes.Equal(a.File, b.File) &&
		bytes.Equal(a.Namespace, b.Namespace) &&
		a.Level == b.Level &&
		bytes.Equal(a.Message, b.Message)
}

func Test_parseFile(t *testing.T) {
	if r := parseFile([]byte("/a/b/c/d.go:23: m"), log.Llongfile); !bytes.Equal(r, []byte("/a/b/c/d.go:23")) {
		t.Errorf("Expected /a/b/c/d.go:23 got %s", r)
	}
	if r := parseFile([]byte("non file input"), log.Llongfile); nil != r {
		t.Errorf("Expected nil got %+v", r)
	}
	if r := parseFile([]byte("/a/b/c/d.go:23: m"), 0); nil != r {
		t.Errorf("Expected nil got %+v", r)
	}
}

func Test_parseLevel(t *testing.T) {
	if lvl, lvll := parseLevel([]byte("info: message"), levels, 0); lvl != 3 || lvll != 6 {
		t.Errorf("Expected 3, 6 got %d, %d", lvl, lvll)
	}
	if lvl, lvll := parseLevel([]byte("unkn: message"), levels, 0); lvl != 0 || lvll != 0 {
		t.Errorf("Expected 0, 0 got %d, %d", lvl, lvll)
	}
	if lvl, lvll := parseLevel([]byte("debug: message"), levels, 0); lvl != 1 || lvll != 7 {
		t.Errorf("Expected 1, 7 got %d, %d", lvl, lvll)
	}
}

func Test_parseNs(t *testing.T) {
	if ns := parseNamespace([]byte("[mleko.arlog] info: message")); !bytes.Equal(ns, []byte("mleko.arlog")) {
		t.Errorf("Expected mleko.arlog got %s", ns)
	}
}
