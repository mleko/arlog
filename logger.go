package arlog

import (
	"strings"
	"errors"
)

type ArLog struct {
	Parser   Parser
	Filters  []Filter
	Handlers []Handler
}

func (l *ArLog) Write(p []byte) (n int, err error) {
	e := l.Parser.Parse(p)
	e = filterEntry(e, l.Filters)
	if (nil != e) {
		handleEntry(e, l.Handlers)
	}
	return len(p), nil
}

func filterEntry(e *LogEntry, filters []Filter) *LogEntry {
	for _, f := range filters {
		e = f.Filter(e)
		if (nil == e) {
			return nil
		}
	}
	return e
}
func handleEntry(e *LogEntry, handlers []Handler) error {
	var ers []string = nil
	for _, h := range handlers {
		er := h.Handle(e)
		if (nil != er) {
			ers = append(ers, er.Error())
		}
	}
	if (nil != ers) {
		return errors.New("Log errors: " + strings.Join(ers, ", "))
	}
	return nil
}

func NewLogger(p Parser, f []Filter, h []Handler) *ArLog {
	return &ArLog{
		Parser:p,
		Filters:f,
		Handlers:h,
	}
}
