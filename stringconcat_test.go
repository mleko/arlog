package arlog

import (
	"testing"
	"fmt"
)

func Benchmark_add(b *testing.B) {
	date := []byte("2009/01/23 01:23:23.123123")
	file := []byte("/a/b/c/d.go:23")
	ns := "[ns] "
	lvl := "info:"
	msg := []byte("message")

	for n := 0; n < b.N; n++ {
		a := string(date) + " " + string(file) + " " + ns + " " + lvl + " " + string(msg)
		if ("" == a) {
			b.Fail()
		}
	}
}

func Benchmark_fmt(b *testing.B) {
	date := []byte("2009/01/23 01:23:23.123123")
	file := []byte("/a/b/c/d.go:23")
	ns := "[ns] "
	lvl := "info:"
	msg := []byte("message")

	for n := 0; n < b.N; n++ {
		a := fmt.Sprintf("%s %s %s %s %s", date, file, ns, lvl, msg, )
		if ("" == a) {
			b.Fail()
		}
	}
}
