package arlog

type stdFormatter struct {
	levels LevelNames
}

func (r *stdFormatter)Format(e *LogEntry) string {
	d := ""
	if (nil != e.DateTime) {
		d = string(e.DateTime) + " "
	}
	f := ""
	if (nil != e.File) {
		f = string(e.File) + ": "
	}
	ns := ""
	if (nil != e.Namespace) {
		ns = "[" + string(e.Namespace) + "] "
	}
	lvl := ""
	if l, ok := r.levels[e.Level]; ok {
		lvl = l + ": "
	}
	return d + f + ns + lvl + string(e.Message)
}

func NewStdFormatter(levels LevelNames) Formatter {
	return &stdFormatter{
		levels: levels,
	}
}
