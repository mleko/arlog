package arlog

import (
	"log"
	"regexp"
)

type stdParser struct {
	flags    int
	levels   Levels
	defLevel Level
}

func (p *stdParser)Parse(b []byte) *LogEntry {
	return parseEntry(b, p.flags, p.levels, p.defLevel)
}

func NewStdParser(flags int, levels Levels, defLevel Level) Parser {
	return &stdParser{
		flags:flags,
		levels:levels,
		defLevel:defLevel,
	}
}

func parseEntry(e []byte, flags int, levels Levels, defaultLevel Level) *LogEntry {
	dt := parseDate(e, flags)
	if (nil != dt) {
		e = e[len(dt) + 1:]
	}

	file := parseFile(e, flags)
	if (nil != file) {
		e = e[len(file) + 2:]
	}

	ns := parseNamespace(e)
	if (nil != ns) {
		e = e[len(ns) + 3:]
	}

	lvl, lvll := parseLevel(e, levels, defaultLevel)
	if (0 != lvll) {
		e = e[lvll:]
	}

	return &LogEntry{
		DateTime: dt,
		File: file,
		Namespace: ns,
		Level: lvl,
		Message: e,
	}
}

func parseDate(l []byte, flags int) []byte {
	dl := dateLength(flags)
	if (0 == dl) {
		return nil
	}
	return l[:dl]
}

func dateLength(flags int) int {
	dl := 0
	if (flags & log.Ldate != 0) {
		dl += 10
		if (flags & (log.Ltime | log.Lmicroseconds) != 0) {
			dl += 1
		}
	}
	if (flags & log.Lmicroseconds != 0) {
		dl += 15
	} else if (flags & log.Ltime != 0) {
		dl += 8
	}
	return dl
}

var fileRegexp *regexp.Regexp

func init() {
	fileRegexp = regexp.MustCompile(`^.*?\.go:[0-9]+: `)
}

func parseFile(l []byte, flags int) []byte {
	if (flags & (log.Llongfile | log.Lshortfile) == 0) {
		return nil;
	}
	if match := fileRegexp.Find(l); nil != match {
		return match[0: len(match) - 2]
	}
	return nil
}

var levelRegex *regexp.Regexp

func init() {
	levelRegex = regexp.MustCompile(`^\S+: `)
}

func parseLevel(l []byte, levels Levels, defaultLevel Level) (Level, int) {
	lName := levelRegex.Find(l)
	if (nil == lName) {
		return defaultLevel, 0
	}
	lvl, in := levels[string(lName[:len(lName)-2])]
	if (in) {
		return lvl, len(lName)
	}
	return defaultLevel, 0
}

var nsRegexp *regexp.Regexp

func init() {
	nsRegexp = regexp.MustCompile(`^\[\S+] `);
}

func parseNamespace(l []byte) []byte {
	if match := nsRegexp.Find(l); match != nil {
		return match[1:len(match) - 2]
	}
	return nil
}
