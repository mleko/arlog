package arlog

import (
	"io"
)

type writerHandler struct {
	wr io.Writer
	f  Formatter
}

func (h writerHandler)Handle(e *LogEntry) error {
	_, err := h.wr.Write([]byte(h.f.Format(e)))
	return err
}

func NewWriterHandler(wr io.Writer, f Formatter) Handler {
	return &writerHandler{
		wr:wr,
		f:f,
	}
}
