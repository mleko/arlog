package arlog

import (
	"testing"
)

var l = LevelNames{
	1:"debug",
	2:"verbose",
	3:"info",
}
var stdFmt = stdFormatter{levels: l}

var testcases = []struct {
	in  *LogEntry
	out string
}{
	{
		&LogEntry{
			DateTime: []byte("2009/01/23 01:23:23.123123"),
			File: []byte("/a/b/c/d.go:23"),
			Namespace: []byte("namespace"),
			Level: 3,
			Message: []byte("message"),
		},
		"2009/01/23 01:23:23.123123 /a/b/c/d.go:23: [namespace] info: message",
	}, {
		&LogEntry{
			Namespace: []byte("namespace"),
			Level: 3,
			Message: []byte("message"),
		},
		"[namespace] info: message",
	}, {
		&LogEntry{
			Message: []byte("message"),
		},
		"message",
	},
}

func TestStdFormatter_Format(t *testing.T) {
	for _, tc := range testcases {
		a := stdFmt.Format(tc.in)
		if a != tc.out {
			t.Errorf("Expected \n%s\n got \n%s", tc.out, a)
		}
	}
}
