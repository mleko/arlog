package filter

import "github.com/mleko/arlog"

type LevelFilter struct {
	minLevel arlog.Level
}

func (f *LevelFilter)Filter(e *arlog.LogEntry) *arlog.LogEntry{
	if (e.Level >= f.minLevel) {
		return e
	}
	return nil
}

func NewLevelFilter(l arlog.Level) *LevelFilter {
	return &LevelFilter{
		minLevel:l,
	}
}
