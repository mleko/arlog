package arlog

import (
	"log"
	"os"
)

func Register() *ArLog{
	stdHandler := NewWriterHandler(os.Stderr, NewStdFormatter(levelNames))
	handlers := []Handler{stdHandler}
	l := NewLogger(
		NewStdParser(log.Flags(), levels, 3),
		nil,
		handlers,
	)
	log.SetOutput(l)
	return l
}
