package arlog

import "math"

const (
	_ = Level(iota)
	DEBUG = Level(iota)
	VERBOSE = Level(iota)
	INFO = Level(iota)
	WARNING = Level(iota)
	ERROR = Level(iota)
	ALL = Level(math.MinInt32)
	NONE = Level(math.MaxInt32)
)

var levels = Levels{
	"debug": DEBUG,
	"dbg": DEBUG,
	"verbose": VERBOSE,
	"info": INFO,
	"warning": WARNING,
	"warn": WARNING,
	"error": ERROR,
	"err": ERROR,
}

var levelNames = LevelNames{
	DEBUG: "debug",
	VERBOSE: "verbose",
	INFO: "info",
	WARNING: "warning",
	ERROR: "error",
}
